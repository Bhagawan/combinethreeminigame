package com.example.combinethreeminigame

import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.os.Build
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.combinethreeminigame.screens.Screens
import com.example.combinethreeminigame.util.*
import com.onesignal.OneSignal
import com.squareup.picasso.Picasso
import com.squareup.picasso.Target
import kotlinx.coroutines.Job
import kotlinx.coroutines.async
import kotlinx.coroutines.launch
import java.text.SimpleDateFormat
import java.util.*

class CombineThreeMainViewModel: ViewModel() {
    private var request: Job? = null
    private var backDownload: Job? = null

    // Public

    fun init(service: String, id: String) {
        request = viewModelScope.async {
            requestSplash(service, id)
        }
    }

    fun retryDownload() {
        Navigator.navigateTo(Screens.SPLASH_SCREEN)
        downloadAssets()
    }

    /// Private

    private suspend fun requestSplash(service: String, id: String) {
        try {
            val time = SimpleDateFormat("z", Locale.getDefault()).format(
                Calendar.getInstance(
                    TimeZone.getTimeZone("GMT"), Locale.getDefault()
                ).time
            )
                .replace("GMT", "")
            val splash = CombineThreeServerClient.create().getSplash(
                Locale.getDefault().language,
                service,
                "${Build.BRAND} ${Build.MODEL}",
                if (time.contains(":")) time else "default",
                id
            )
            viewModelScope.launch {
                if (splash.isSuccessful) {
                    if (splash.body() != null) {
                        when (splash.body()!!.url) {
                            "no" -> downloadAssets()
                            "nopush" -> {
                                OneSignal.disablePush(true)
                                downloadAssets()
                            }
                            else -> viewModelScope.launch {
                                CurrentAppData.url = "https://${splash.body()!!.url}"
                                Navigator.navigateTo(Screens.WEB_VIEW)
                            }
                        }
                    } else downloadAssets()
                } else downloadAssets()
            }
        } catch (e: Exception) {
            downloadAssets()
        }
    }

    private lateinit var backTar: Target
    private lateinit var topBarTarget: Target
    private val ballTargets = ArrayList<Target>()
    private fun downloadAssets() {
        if(backDownload?.isActive == true) {
            backDownload?.cancel()
        }
        backDownload = viewModelScope.launch {
            val picasso = Picasso.get()

            backTar = object : Target {
                override fun onPrepareLoad(placeHolderDrawable: Drawable?) { }
                override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                    switchToErrorScreen()
                }
                override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                    if(bitmap != null) {
                        CurrentAppData.back = bitmap
                        if(CurrentAppData.allDownloaded()) switchToGame()
                    } else {
                        switchToErrorScreen()
                    }
                }
            }
            picasso.load(UrlBack).into(backTar)
        }
        viewModelScope.launch {
            val picasso = Picasso.get()

            topBarTarget = object : Target {
                override fun onPrepareLoad(placeHolderDrawable: Drawable?) { }
                override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                    switchToErrorScreen()
                }
                override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                    if(bitmap != null) {
                        CurrentAppData.topBar = bitmap
                        if(CurrentAppData.allDownloaded()) switchToGame()
                    } else {
                        switchToErrorScreen()
                    }
                }
            }
            picasso.load(UrlTopBar).into(topBarTarget)
        }
        loadBall("Football", UrlFootball)
        loadBall("Basketball", UrlBasketball)
        loadBall("American", UrlAmerican)
        loadBall("Volleyball", UrlVolleyball)
        loadBall("Tennis", UrlTennis)
    }

    private fun loadBall(name: String, url: String) {
        val target = object : Target {
            override fun onPrepareLoad(placeHolderDrawable: Drawable?) { }
            override fun onBitmapFailed(e: Exception?, errorDrawable: Drawable?) {
                switchToErrorScreen()
            }
            override fun onBitmapLoaded(bitmap: Bitmap?, from: Picasso.LoadedFrom?) {
                if(bitmap != null) {
                    CurrentAppData.balls.add(Ball(name, bitmap))
                    if(CurrentAppData.allDownloaded()) switchToGame()
                } else {
                    switchToErrorScreen()
                }
            }
        }
        ballTargets.add(target)
        viewModelScope.launch {
            Picasso.get().load(url).into(target)
        }
    }

    private fun switchToGame() {
        Navigator.navigateTo(Screens.GAME_SCREEN)
    }

    private fun switchToErrorScreen() {
        Navigator.navigateTo(Screens.ASSETS_LOADING_ERROR_SCREEN)
    }
}