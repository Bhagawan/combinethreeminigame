package com.example.combinethreeminigame.screens.gameScreen

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch

class CombineThreeViewModel: ViewModel() {
    private val _resultPopup = MutableStateFlow(false)
    val resultPopup = _resultPopup.asStateFlow()

    private val _pointsFlow = MutableStateFlow(0)
    val pointsFlow = _pointsFlow.asStateFlow()

    private val _restartFlow = MutableSharedFlow<Boolean>()
    val restartFlow = _restartFlow.asSharedFlow()

    fun restart() {
        _resultPopup.tryEmit(false)
        viewModelScope.launch {
            _restartFlow.emit(true)
        }
    }

    fun end() {
        _resultPopup.tryEmit(true)
    }

    fun setPoints(amount: Int) {
        _pointsFlow.tryEmit(amount)
    }
}