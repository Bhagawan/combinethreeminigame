package com.example.combinethreeminigame.screens.gameScreen

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.asImageBitmap
import androidx.compose.ui.layout.ContentScale
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.TextUnit
import androidx.compose.ui.unit.TextUnitType
import androidx.compose.ui.viewinterop.AndroidView
import androidx.lifecycle.viewModelScope
import androidx.lifecycle.viewmodel.compose.viewModel
import com.example.combinethreeminigame.R
import com.example.combinethreeminigame.ui.theme.Blue
import com.example.combinethreeminigame.ui.theme.Gold
import com.example.combinethreeminigame.ui.theme.Grey
import com.example.combinethreeminigame.util.CurrentAppData
import com.example.combinethreeminigame.view.CombineThreeView
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach

@Preview
@Composable
fun GameScreen() {
    val viewModel = viewModel<CombineThreeViewModel>()
    val points by viewModel.pointsFlow.collectAsState(initial = 0)
    Box(modifier = Modifier.fillMaxSize(), contentAlignment = Alignment.Center) {
        CurrentAppData.back?.let {
            Image(it.asImageBitmap(), contentDescription = "field", modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds)
        }
        Column(modifier = Modifier.fillMaxSize(), horizontalAlignment = Alignment.CenterHorizontally, verticalArrangement = Arrangement.Center) {
            Box(modifier = Modifier
                .weight(1.0f, true)
                .fillMaxWidth(), contentAlignment = Alignment.Center) {
                CurrentAppData.topBar?.let { Image(it.asImageBitmap(), contentDescription = "topBar", modifier = Modifier.fillMaxSize(), contentScale = ContentScale.FillBounds) }
                Text(text = points.toString(), fontSize = TextUnit(25.0f, TextUnitType.Sp), color = Gold, textAlign = TextAlign.Center, modifier = Modifier.padding(bottom = Dp(5.0f)))

                Button(onClick = viewModel::restart, contentPadding = PaddingValues(Dp(3.0f)), colors = ButtonDefaults.buttonColors(backgroundColor = Grey, contentColor = Blue), modifier = Modifier.align(
                    Alignment.CenterEnd).aspectRatio(1.0f).padding(Dp(10.0f))) {
                    Image(painter = painterResource(id = R.drawable.ic_baseline_reload), contentDescription = stringResource(id = R.string.desc_reset), contentScale = ContentScale.Fit)
                }

            }
            Box(modifier = Modifier
                .weight(11.0f, true)
                .fillMaxWidth()) {
                AndroidView(factory = { CombineThreeView(it) },
                    modifier = Modifier.fillMaxSize(),
                    update = { view ->
                        viewModel.restartFlow.onEach {
                            view.restart()
                        }.launchIn(viewModel.viewModelScope)
                        view.setInterface(object: CombineThreeView.CombineThreeInterface {
                            override fun pointsUpdate(amount: Int) {
                                viewModel.setPoints(amount)
                            }

                            override fun onEnd() {
                                viewModel.end()
                            }
                        })
                    })
            }
        }
    }

    val endPopup = viewModel.resultPopup.collectAsState(false)

    if(endPopup.value) CombineThreeEndPopup(points, viewModel::restart)
}