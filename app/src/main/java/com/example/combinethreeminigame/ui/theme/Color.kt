package com.example.combinethreeminigame.ui.theme

import androidx.compose.ui.graphics.Color

val Purple200 = Color(0xFFBB86FC)
val Purple500 = Color(0xFF6200EE)
val Purple700 = Color(0xFF3700B3)
val Teal200 = Color(0xFF03DAC5)

val Orange = Color(0xFF94512C)
val Grey_light = Color(0xFF8B7F7F)
val Grey = Color(0xFF494444)
val Blue = Color(0xFF247EBB)
val Red = Color(0xFFA72E2E)
val Gold = Color(0xFFE6A23F)
val Grey_transparent = Color(0x80000000)