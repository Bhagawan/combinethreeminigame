package com.example.combinethreeminigame.util

import android.graphics.Bitmap

data class Ball(val name: String, val bitmap: Bitmap)
