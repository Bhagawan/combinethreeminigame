package com.example.combinethreeminigame.util

import androidx.annotation.Keep

@Keep
data class CombineThreeSplashResponse(val url : String)