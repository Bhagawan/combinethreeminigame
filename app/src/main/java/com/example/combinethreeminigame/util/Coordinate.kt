package com.example.combinethreeminigame.util

data class Coordinate(var x: Int, var y: Int)
