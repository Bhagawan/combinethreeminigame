package com.example.combinethreeminigame.util

import android.graphics.Bitmap

object CurrentAppData {
    val balls = ArrayList<Ball>()
    var url = ""
    var back : Bitmap? =  null
    var topBar: Bitmap? = null

    fun allDownloaded(): Boolean = balls.size >= 5 && back != null && topBar != null
}