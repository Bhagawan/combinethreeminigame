package com.example.combinethreeminigame.util

const val UrlSplash = "CombineThreeMiniGame/splash.php"
const val UrlBack = "http://195.201.125.8/CombineThreeMiniGame/back.png"
const val UrlLogo = "http://195.201.125.8/CombineThreeMiniGame/assets/football.png"
const val UrlTopBar = "http://195.201.125.8/CombineThreeMiniGame/topBar.png"
const val UrlFootball = "http://195.201.125.8/CombineThreeMiniGame/assets/football.png"
const val UrlBasketball = "http://195.201.125.8/CombineThreeMiniGame/assets/basketball.png"
const val UrlAmerican = "http://195.201.125.8/CombineThreeMiniGame/assets/american_ball.png"
const val UrlVolleyball = "http://195.201.125.8/CombineThreeMiniGame/assets/volleyball.png"
const val UrlTennis = "http://195.201.125.8/CombineThreeMiniGame/assets/tennisBall.png"