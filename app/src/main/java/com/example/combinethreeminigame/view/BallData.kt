package com.example.combinethreeminigame.view

import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.Rect
import com.example.combinethreeminigame.util.Ball
import com.example.combinethreeminigame.util.Coordinate
import kotlin.math.absoluteValue
import kotlin.math.sign

class BallData(private var ball: Ball, private var x: Int, private var y: Int, private val size: Int) {
    private var tempX: Int = x
    private var tempY: Int = y

    private var targetX = x
    private var targetY = y

    private val moveSpeed = size / 5

    fun update() {
        if(targetX != tempX) tempX += ((targetX - tempX).sign * moveSpeed).coerceAtMost((targetX - tempX).absoluteValue).coerceAtLeast((targetX - tempX).absoluteValue * -1)
        if(targetY != tempY) tempY += ((targetY - tempY).sign * moveSpeed).coerceAtMost((targetY - tempY).absoluteValue).coerceAtLeast((targetY - tempY).absoluteValue * -1)
    }

    fun draw(c: Canvas) {
        val r = size / 2.0f
        c.drawBitmap(ball.bitmap, null, Rect((tempX - r).toInt(), (tempY - r).toInt(), (tempX + r).toInt(), (tempY + r).toInt()), Paint())
    }

    fun setX(newX: Int) {
        tempX = newX
        targetX = tempX
    }

    fun setY(newY: Int) {
        tempY = newY
        targetY = tempY
    }

    fun moveUp() {
        targetY = y - size
    }

    fun moveRight() {
        targetX = x + size
    }

    fun moveDown() {
        targetY = y + size
    }

    fun moveLeft() {
        targetX = x - size
    }

    fun moveBack() {
        targetX = x
        targetY = y
    }

    fun putBallOn(newX: Int, newY: Int) {
        tempX = newX
        tempY = newY
        targetX = x
        targetY = y
    }

    fun isMoved(): Boolean = targetX != x || targetY != y

    fun setBall(newBallData: BallData) {
        ball = newBallData.ball
        targetX = x
        targetY = y
        tempX = newBallData.tempX
        tempY = newBallData.tempY
    }

    fun isSame(ballData: BallData) : Boolean = ball.name == ballData.ball.name

    fun getInitialCoordinate(): Coordinate = Coordinate(x, y)

    fun getCurrentCoordinate(): Coordinate = Coordinate(tempX, tempY)

}