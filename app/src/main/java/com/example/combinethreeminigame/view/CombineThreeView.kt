package com.example.combinethreeminigame.view

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Canvas
import android.view.MotionEvent
import android.view.View
import com.example.combinethreeminigame.util.Coordinate
import com.example.combinethreeminigame.util.CurrentAppData
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.math.absoluteValue

class CombineThreeView(context: Context): View(context){
    private var mWidth = 0
    private var mHeight = 0
    private var ballSize = 1.0f

    private var points = 0

    companion object {
        const val STATE_ANIMATION = 0
        const val STATE_GAME = 1
        const val STATE_PAUSE = 2
    }

    private lateinit var field: List<List<BallData>>

    private var state = STATE_PAUSE

    private var mInterface: CombineThreeInterface? = null

    private var selectedBall: Coordinate? = null

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            ballSize = mWidth / 5.0f
            createField()
        }
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            if(state == STATE_ANIMATION) updateAnimation()
            updateBalls()
            drawBalls(it)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                when(state) {
                    STATE_GAME -> selectBall(event.x, event.y)
                    else -> { }
                }
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                when(state) {
                    STATE_GAME -> moveBall(event.x, event.y)
                    else -> {}
                }
                return true
            }
            MotionEvent.ACTION_UP -> {
                when(state) {
                    STATE_GAME -> finishMove(event.x, event.y)
                }
                return true
            }
        }
        return super.onTouchEvent(event)
    }

    /// Public

    fun setInterface(i: CombineThreeInterface) {
        mInterface = i
    }

    fun restart() {
        createField()
        state = STATE_GAME
        points = 0
        mInterface?.pointsUpdate(points)
    }

    //// Private

    private fun drawBalls(c: Canvas) {
        for(column in field) for(ball in column) ball.draw(c)
    }

    private fun updateBalls() {
        for(column in field) for(ball in column) ball.update()
        if(!movesPossible() && state == STATE_GAME) {
            state = STATE_PAUSE
            mInterface?.onEnd()
        }
    }

    private fun updateAnimation() {
        var ended = true
        for(column in field) for(ball in column) {
            if(ball.isMoved()) ended = false
        }
        if(ended) {
            if(fieldContainsFullElements(field)) removeMatched()
            else if(movesPossible()) {
                state = STATE_GAME
            }
            else if(state != STATE_PAUSE) {
                state = STATE_PAUSE
                mInterface?.onEnd()
            }
        }
    }

    private fun selectBall(x: Float, y: Float) {
        val topOffset = mHeight - ballSize.toInt() * (mHeight / ballSize.toInt())
        selectedBall = Coordinate(x.toInt() / ballSize.toInt(), (y - topOffset).toInt() / ballSize.toInt())
    }

    private fun moveBall(x: Float, y: Float) {
        selectedBall?.let {
            val initial = field[it.x][it.y].getInitialCoordinate()
            val horizontal = (initial.x - x).absoluteValue > (initial.y - y).absoluteValue
            val targetPlace = if (horizontal) Coordinate((it.x + (x - initial.x).toInt() / ballSize.toInt()).coerceAtLeast(0).coerceAtMost(field.size - 1), it.y)
            else Coordinate(it.x, (it.y + (y - initial.y).toInt() / ballSize.toInt()).coerceAtLeast(0).coerceAtMost(field[0].size - 1))
            if(horizontal) {
                field[it.x][it.y].setX(x.coerceIn(ballSize / 2.0f, mWidth - ballSize / 2).toInt())
                field[it.x][it.y].setY(initial.y)
            } else {
                field[it.x][it.y].setY(y.coerceIn(field[0][0].getInitialCoordinate().y.toFloat(), mHeight - ballSize / 2).toInt())
                field[it.x][it.y].setX(initial.x)
            }
            for(bX in field.indices) for(bY in field[bX].indices) {
                if(horizontal && bY == it.y) {
                    if(bX > it.x && bX <= targetPlace.x) field[bX][bY].moveLeft()
                    else if(bX < it.x && bX >= targetPlace.x) field[bX][bY].moveRight()
                    else if(bX != it.x || bY != it.y ) {
                        field[bX][bY].moveBack()
                    }
                } else if(!horizontal && bX == it.x) {
                    if(bY > it.y && bY <= targetPlace.y) field[bX][bY].moveUp()
                    else if(bY < it.y && bY >= targetPlace.y) field[bX][bY].moveDown()
                    else if(bX != it.x || bY != it.y ) {
                        field[bX][bY].moveBack()
                    }
                } else field[bX][bY].moveBack()
            }
        }
    }

    private fun finishMove(x: Float, y: Float) {
        moveBall(x, y)
        if(fieldContainsFullElements(createTempField())) {
            putBallOn(x, y)
            removeMatched()
        } else {
            for(bX in field.indices) for(bY in field[bX].indices) field[bX][bY].moveBack()
            state = STATE_ANIMATION
        }
        selectedBall = null
    }

    private fun putBallOn(x: Float, y: Float) {
        selectedBall?.let {
            val ball = BallData(CurrentAppData.balls.random(), 0,0,0).apply { setBall(field[it.x][it.y]) }
            val initial = field[it.x][it.y].getInitialCoordinate()
            val horizontal = (initial.x - x).absoluteValue > (initial.y - y).absoluteValue
            val targetPlace = if (horizontal) Coordinate((it.x + (x - initial.x).toInt() / ballSize.toInt()).coerceAtLeast(0).coerceAtMost(field.size - 1), it.y)
            else Coordinate(it.x, (it.y + (y - initial.y).toInt() / ballSize.toInt()).coerceAtLeast(0).coerceAtMost(field[0].size - 1))
            if(horizontal) {
                if(initial.x < x) {
                    if(it.x < field.size - 1) {
                        for(n in it.x until targetPlace.x) field[n][it.y].setBall(field[n + 1][it.y])
                    }
                } else {
                    if(it.x > 0) for(n in it.x downTo targetPlace.x + 1) field[n][it.y].setBall(field[n - 1][it.y])
                }
            } else {
                if(initial.y < y) {
                    if(it.y < field[0].size - 1) {
                        for(n in it.y until targetPlace.y) field[it.x][n].setBall(field[it.x][n + 1])
                    }
                } else {
                    if(it.y > 0) for(n in it.y downTo targetPlace.y + 1) field[it.x][n].setBall(field[it.x][n - 1])
                }
            }
            field[targetPlace.x][targetPlace.y].setBall(ball)
        }
    }

    private fun fieldContainsFullElements(fieldToCheck: List<List<BallData>>): Boolean {
        for(bX in fieldToCheck.indices) {
            for(bY in fieldToCheck[bX].indices) {
                if (bX < fieldToCheck.size - 2) {
                    if (fieldToCheck[bX][bY].isSame(fieldToCheck[bX + 1][bY]) && fieldToCheck[bX][bY].isSame(fieldToCheck[bX + 2][bY])) return true
                }
                if (bY < field[0].size - 2) {
                    if (fieldToCheck[bX][bY].isSame(fieldToCheck[bX][bY + 1]) && fieldToCheck[bX][bY].isSame(fieldToCheck[bX][bY + 2])) return true
                }
            }
        }
        return false
    }

    private fun createTempField(): List<List<BallData>> {
        return if(selectedBall != null) {
            val topOffset = mHeight - ballSize.toInt() * (mHeight / ballSize.toInt())
            val fieldToCheck = ArrayList<ArrayList<BallData>>()
            val ball = field[selectedBall!!.x][selectedBall!!.y]
            val current = ball.getCurrentCoordinate()
            val currentField = Coordinate(current.x / ballSize.toInt(), (current.y - topOffset) / ballSize.toInt())
            val horizontal = (ball.getInitialCoordinate().x - current.x).absoluteValue > (ball.getInitialCoordinate().y - current.y).absoluteValue
            for(x in field.indices) {
                val column = ArrayList<BallData>()
                for(y in field[x].indices) {
                    if(x == currentField.x && y == currentField.y) {
                        column.add(ball)
                    } else if(!horizontal && x == selectedBall!!.x) {
                        if (y < currentField.y && y >= selectedBall!!.y) {
                            column.add(field[x][y + 1])
                        } else if(y > currentField.y && y <= selectedBall!!.y) {
                            column.add(field[x][y - 1])
                        } else column.add(field[x][y])
                    } else if(horizontal && y == selectedBall!!.y) {
                        if (x < currentField.x && x >= selectedBall!!.x) {
                            column.add(field[x + 1][y])
                        } else if(x > currentField.x && x <= selectedBall!!.x) {
                            column.add(field[x - 1][y])
                        } else column.add(field[x][y])
                    } else column.add(field[x][y])
                }
                fieldToCheck.add(column)
            }
            fieldToCheck.toList()
        } else field
    }

    private fun removeMatched() {
        state = STATE_ANIMATION
        val toRemove = ArrayList<Coordinate>()
        val topOffset = mHeight - ballSize.toInt() * (mHeight / ballSize.toInt())
        for(bX in field.indices) {
            for(bY in field[bX].indices) {
                val horizontal = ArrayList<BallData>()
                if (bX < field.size - 2) {
                    horizontal.add(field[bX][bY])
                    for(t in bX + 1 until field.size) if(field[t][bY].isSame(field[bX][bY])) {
                        horizontal.add(field[t][bY])
                    } else break
                }
                val vertical = ArrayList<BallData>()
                if (bY < field[0].size - 2) {
                    vertical.add(field[bX][bY])
                    for(t in bY + 1 until field[0].size) if(field[bX][t].isSame(field[bX][bY])) {
                        vertical.add(field[bX][t])
                    } else break
                }
                if(horizontal.size > 2) {
                    for(ball in horizontal) {
                        val c = Coordinate(ball.getCurrentCoordinate().x / ballSize.toInt(), (ball.getCurrentCoordinate().y - topOffset) / ballSize.toInt())
                        if(!toRemove.contains(c)) toRemove.add(c)
                    }
                }
                if(vertical.size > 2) {
                    for(ball in vertical) {
                        val c = Coordinate(ball.getCurrentCoordinate().x / ballSize.toInt(), (ball.getCurrentCoordinate().y - topOffset) / ballSize.toInt())
                        if(!toRemove.contains(c)) toRemove.add(c)
                    }
                }
            }
        }
        for(n in 0..toRemove.size) points+= n*10
        mInterface?.pointsUpdate(points)
        for(ball in toRemove) removeBallAt(ball.x, ball.y)
    }

    private fun removeBallAt(x: Int, y: Int) {
        if(x in field.indices && y in field[0].indices) {
            if(y > 0) for(t in y downTo 1) {
                field[x][t].setBall(field[x][t - 1])
            }
            val c = field[x][0].getInitialCoordinate()
            val randomBall = BallData(CurrentAppData.balls.random(), c.x, c.y, ballSize.toInt())
            randomBall.putBallOn((ballSize / 2.0f + x * ballSize).toInt(), (-ballSize / 2).toInt().coerceAtMost(field[x][1].getCurrentCoordinate().y - ballSize.toInt()))
            field[x][0].setBall(randomBall)
        }
    }

    private fun movesPossible(): Boolean {
        for(bX in field.indices) {
            for(bY in field[bX].indices) {
                for(n in field.indices) {
                    if(n != bX) {
                        if(checkTempFieldOnMovePossibility(bX, bY,n - bX, 0)) {
                            return true
                        }
                    }
                }
                for(n in field[0].indices) {
                    if(n != bY) {
                        if(checkTempFieldOnMovePossibility(bX, bY,0, n - bY)) {
                            return true
                        }
                    }
                }
            }
        }
        return false
    }

    private fun checkTempFieldOnMovePossibility(x: Int, y: Int, xOff: Int, yOff: Int): Boolean {
       if(x in field.indices && y in field[0].indices && x + xOff in field.indices && y + yOff in field[0].indices) {
           if(xOff != 0) {
               val newField = ArrayList<ArrayList<BallData>>()
               for(tX in field.indices) {
                   val column = ArrayList<BallData>()
                   for(tY in field[x].indices) {
                      if(tY == y) {
                          if(xOff > 0) {
                              if(tX >= x && tX < x + xOff) column.add(field[tX + 1][tY])
                              else if(tX == x + xOff) column.add(field[x][y])
                              else column.add(field[tX][tY])
                          } else {
                              if(tX <= x && tX > x + xOff) column.add(field[tX - 1][tY])
                              else if(tX == x + xOff) column.add(field[x][y])
                              else column.add(field[tX][tY])
                          }
                      } else column.add(field[tX][tY])
                   }
                   newField.add(column)
               }
               return fieldContainsFullElements(newField)
           } else if(yOff != 0) {
               val newField = ArrayList<ArrayList<BallData>>()
               for(tX in field.indices) {
                   val column = ArrayList<BallData>()
                   for(tY in field[x].indices) {
                       if(tX == x) {
                           if(yOff > 0) {
                               if(tY >= y && tY < y + yOff) column.add(field[tX][tY + 1])
                               else if(tY == y + yOff) column.add(field[x][y])
                               else column.add(field[tX][tY])
                           } else {
                               if(tY <= y && tY > y + yOff) column.add(field[tX][tY - 1])
                               else if(tY == y + yOff) column.add(field[x][y])
                               else column.add(field[tX][tY])
                           }
                       } else column.add(field[tX][tY])
                   }
                   newField.add(column)
               }
               return fieldContainsFullElements(newField)
           } else return fieldContainsFullElements(field)
       } else return false
    }

    private fun createField() {
        val topOffset = mHeight - ballSize.toInt() * (mHeight / ballSize.toInt())
        field = List(mWidth / ballSize.toInt()) { x ->  List(mHeight / ballSize.toInt()) { y -> BallData(CurrentAppData.balls.random(), (ballSize / 2.0f + x * ballSize).toInt(), (topOffset + ballSize / 2.0f + y * ballSize).toInt(), ballSize.toInt()) } }
        if(!movesPossible()) createField()
        else state = STATE_GAME
    }

    interface CombineThreeInterface {
        fun pointsUpdate(amount: Int)
        fun onEnd()
    }
}